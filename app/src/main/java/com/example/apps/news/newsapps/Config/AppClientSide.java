package com.example.apps.news.newsapps.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RONIJR on 7/24/18.
 */

public class AppClientSide {

    public static final String BASE_URL = "https://newsapi.org/v2/";
    private Retrofit retrofit;
    public Retrofit getClient(){
        OkHttpClient okHttpClient =new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30,TimeUnit.SECONDS)
                .writeTimeout(15,TimeUnit.SECONDS)
                .build();

        if(retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();
        }

        return retrofit;

    }
}
