package com.example.apps.news.newsapps.Config;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import retrofit2.Response;

/**
 * Created by RONIJR on 7/24/18.
 */

public class GlobalConfig {


    private Context context;

    public GlobalConfig(Context context){
        this.context = context;
    }

    public String errorMessage(Throwable throwable){
        String errorMsg = "Internal Server Error";
        if(!isNetworkConnceted()){
            errorMsg = "Nothing Internet Connection";
        } else if(throwable instanceof TimeoutException){
            errorMsg = "Oops, Connection Timeout";
        }
        return errorMsg;
    }

    public void setMessageErro(Response response){
        try {
            String data = response.errorBody().string();
            try {
                JSONObject message = new JSONObject(data);
                Toast.makeText(context, message.getString("message"), Toast.LENGTH_SHORT).show();
            }catch (JSONException e){
                e.printStackTrace();

            }
        } catch (IOException e) {
            e.printStackTrace();

        }
    }


    private boolean isNetworkConnceted(){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;
    }
}
