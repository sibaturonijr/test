package com.example.apps.news.newsapps.View;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.example.apps.news.newsapps.Adapter.AdapterVendorNews;
import com.example.apps.news.newsapps.Config.AppClientSide;
import com.example.apps.news.newsapps.Config.GlobalConfig;
import com.example.apps.news.newsapps.Model.VendorNews.ResponseVendorNews;
import com.example.apps.news.newsapps.Model.VendorNews.SourcesItem;
import com.example.apps.news.newsapps.R;
import com.example.apps.news.newsapps.Root.Root;

import java.util.HashMap;
import java.util.List;

import javax.microedition.khronos.opengles.GL;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.swipe) SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.toolbar) android.support.v7.widget.Toolbar toolbar;

    //Calling GlobalConfig
    GlobalConfig globalConfig;

    //Calling Adapter for ferch
    AdapterVendorNews adapterVendorNews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        inisialisasiView();
    }


    private void inisialisasiView(){
        getNewsVendor();

        //SetSwipe
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewsVendor();
            }
        });


        //Instance Global Config
        globalConfig = new GlobalConfig(this);

        //Set recyclerView
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manage = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manage);

        inisialisasiToolbar();
    }

    private void inisialisasiToolbar(){
        setSupportActionBar(toolbar);
        toolbar.setTitle("News Apps");
    }

    private void getNewsVendor(){
        final Root root = new AppClientSide().getClient().create(Root.class);
        HashMap<String ,String> params = new HashMap<>();
        params.put("apiKey","fe2bb0876be4494f82f399253cc681e6");
        Call<ResponseVendorNews> call = root.getVendorNews(params);
        swipeRefresh.setRefreshing(true);
        call.enqueue(new Callback<ResponseVendorNews>() {
            @Override
            public void onResponse(Call<ResponseVendorNews> call, Response<ResponseVendorNews> response) {
                ResponseVendorNews data = response.body();
                if(response.isSuccessful()){
                    if(!data.getSources().isEmpty()){
                        setAdapterVendorNews(data.getSources());
                    } else {
                        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.empty_data,null,false);
                        swipeRefresh.addView(view);
                    }
                } else{
                    globalConfig.setMessageErro(response);
                }
                swipeRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ResponseVendorNews> call, Throwable t) {
                Toast.makeText(MainActivity.this, globalConfig.errorMessage(t), Toast.LENGTH_SHORT).show();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    private void setAdapterVendorNews(List<SourcesItem> sourcesItems){
        //Set Adapter
        adapterVendorNews = new AdapterVendorNews(this, sourcesItems);
        recyclerView.setAdapter(adapterVendorNews);
    }
}
