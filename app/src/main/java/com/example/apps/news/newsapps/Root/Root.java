package com.example.apps.news.newsapps.Root;

import com.example.apps.news.newsapps.Model.Article.ResponseArticle;
import com.example.apps.news.newsapps.Model.VendorNews.ResponseVendorNews;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by RONIJR on 7/24/18.
 */

public interface Root {

    @GET("sources")
    Call<ResponseVendorNews>  getVendorNews(@QueryMap() Map<String, String> map);

    @GET("everything")
    Call<ResponseArticle>  getArticle(@QueryMap() Map<String, String> map);

}
