package com.example.apps.news.newsapps.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apps.news.newsapps.Model.Article.ArticlesItem;
import com.example.apps.news.newsapps.R;
import com.squareup.picasso.Picasso;
import com.thefinestartist.finestwebview.FinestWebView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RONIJR on 7/24/18.
 */

public class AdapterArticle extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ArticlesItem> articlesItems;

    public AdapterArticle(Context context, List<ArticlesItem> articlesItems){
        this.context      = context;
        this.articlesItems = articlesItems;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewItems = (ViewHolder) holder;
        final ArticlesItem items    = articlesItems.get(position);
        String author = items.getAuthor() == null ? "No Name":items.getAuthor();


        //Set Value
        viewItems.titleArticle.setText(items.getTitle());
        viewItems.authorArticle.setText( "Author : "+author);
        Picasso.with(context)
                .load(items.getUrlToImage())
                .into(viewItems.imgView);

        //Set Action Clicked
        viewItems.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FinestWebView.Builder(context).show(items.getUrl());
            }
        });
    }

    @Override
    public int getItemCount() {
        return articlesItems.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_article, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends  RecyclerView.ViewHolder {
        @BindView(R.id.titleArticle) TextView titleArticle;
        @BindView(R.id.authorArticle) TextView authorArticle;
        @BindView(R.id.imageView) ImageView imgView;
        @BindView(R.id.card_view) CardView cardView;
        ViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
