package com.example.apps.news.newsapps.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apps.news.newsapps.Model.VendorNews.SourcesItem;
import com.example.apps.news.newsapps.R;
import com.example.apps.news.newsapps.View.ArticleListActivity;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RONIJR on 7/24/18.
 */

public class AdapterVendorNews extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<SourcesItem> sourcesItems;

    public AdapterVendorNews(Context context, List<SourcesItem> sourcesItems){
        this.context      = context;
        this.sourcesItems = sourcesItems;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewItems = (ViewHolder) holder;
        final SourcesItem items    = sourcesItems.get(position);

        //Breakdown URL in data
        String domains = null;
        try {
            URL url = new URL(items.getUrl());
            String host = url.getHost();
            String[] split = host.split("\\.");
            if(split[0].equals("www")){
                if(split.length > 3){
                    domains = split[1]+"."+split[2]+"."+split[3];
                } else {
                    domains = split[1]+"."+split[2];
                }
            } else{
                domains = host;
            }

            //Get Icon From Web


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }




        //Set Value
        viewItems.nameVendor.setText(items.getName());
        viewItems.deskVendor.setText(items.getDescription());
        viewItems.imgView.setImageDrawable(context.getResources().getDrawable(R.drawable.logo));

        //Set Action Clicked
        final String finalDomains = domains;
        viewItems.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ArticleListActivity.class);
                intent.putExtra("domains", finalDomains);
                intent.putExtra("vendor", items.getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sourcesItems.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_vendor_news, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends  RecyclerView.ViewHolder {
        @BindView(R.id.nameVendor) TextView nameVendor;
        @BindView(R.id.deskVendor) TextView deskVendor;
        @BindView(R.id.imgView) ImageView imgView;
        @BindView(R.id.card_view) CardView cardView;
        ViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }


}
