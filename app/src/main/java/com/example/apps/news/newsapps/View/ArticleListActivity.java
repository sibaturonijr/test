package com.example.apps.news.newsapps.View;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apps.news.newsapps.Adapter.AdapterArticle;
import com.example.apps.news.newsapps.Config.AppClientSide;
import com.example.apps.news.newsapps.Config.GlobalConfig;
import com.example.apps.news.newsapps.Model.Article.ArticlesItem;
import com.example.apps.news.newsapps.Model.Article.ResponseArticle;
import com.example.apps.news.newsapps.R;
import com.example.apps.news.newsapps.Root.Root;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleListActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.swipe) SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.searchTextView) EditText searchTextView;
    @BindView(R.id.toolbar) android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.forlayout_empty) LinearLayout forlayout_empty;

    //Calling GlobalConfig
    GlobalConfig globalConfig;

    //Calling Adapter for ferch
    AdapterArticle adapterArticle;
    String domains,nameVendor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);
        ButterKnife.bind(this);
        inisialisasiView();
    }


    private void inisialisasiView(){
        Intent intent = getIntent();
        domains = intent.getStringExtra("domains");
        nameVendor = intent.getStringExtra("vendor");

        //SetSwipe
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getArticle(domains);
            }
        });

        getArticle(domains);

        //Instance Global Config
        globalConfig = new GlobalConfig(this);

        //Set recyclerView
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manage = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manage);

        //Serach
        setTextSearch();
        inisialisasiToolbar();
    }


    private void inisialisasiToolbar(){
        setSupportActionBar(toolbar);
        setTitle("Article "+ nameVendor);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getArticle(String domains){
        final Root root = new AppClientSide().getClient().create(Root.class);
        HashMap<String ,String> params = new HashMap<>();
        params.put("apiKey","fe2bb0876be4494f82f399253cc681e6");
        params.put("domains",domains);
        if(!searchTextView.getText().toString().isEmpty()){params.put("q",searchTextView.getText().toString().trim());}
        Call<ResponseArticle> call = root.getArticle(params);
        swipeRefresh.setRefreshing(true);
        call.enqueue(new Callback<ResponseArticle>() {
            @Override
            public void onResponse(Call<ResponseArticle> call, Response<ResponseArticle> response) {
                ResponseArticle data = response.body();
                if(response.isSuccessful()){
                    if(!data.getArticles().isEmpty()){
                        setAdapterArticle(data.getArticles());
                        forlayout_empty.setVisibility(View.GONE);
                    } else {
                        ConstraintLayout view = (ConstraintLayout) LayoutInflater.from(ArticleListActivity.this).inflate(R.layout.empty_data,null,false);
                        forlayout_empty.addView(view);
                        forlayout_empty.setVisibility(View.VISIBLE);
                        Log.i("TAG","NOT FOUND");
                    }
                } else{
                    globalConfig.setMessageErro(response);
                }
                swipeRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ResponseArticle> call, Throwable t) {
                Toast.makeText(ArticleListActivity.this, globalConfig.errorMessage(t), Toast.LENGTH_SHORT).show();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    private void setAdapterArticle(List<ArticlesItem> articlesItems){
        //Set Adapter
        adapterArticle = new AdapterArticle(this, articlesItems);
        recyclerView.setAdapter(adapterArticle);
    }


    private void setTextSearch(){
        searchTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                searchTextView.setText(v.getText().toString().trim());
                getArticle(domains);
                hideKeyboard(v);
                return false;
            }
        });
    }

    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
